# Platform #

Node.js and React/Flux CMS platform to render fast, responsive, and mobile first web pages and applications.

### Setup ###

* Install dependencies with npm install
* Build assets with npm run build
* Run app server with npm start
* Build site with existing components or create more in components directory
* Back end currently powered by Mongo document store