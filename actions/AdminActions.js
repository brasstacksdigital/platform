/**
 * AdminActions
 */
var AppDispatcher = require('../dispatcher/AppDispatcher'),
  AdminConstants = require('../constants/AdminConstants');

var SiteActions = {

  loadAdminConfigData: function (data) {
    AppDispatcher.handleAPIAction({
      actionType: AdminConstants.LOAD_ADMIN_CONFIG_DATA,
      data: data
    });
  },

  /**
   * @param {object} view
   */
  updateActiveView: function (view) {
    AppDispatcher.handleViewAction({
      actionType: AdminConstants.UPDATE_ACTIVE_VIEW,
      view: view,
    });
  }

};

module.exports = SiteActions;
