/**
 * SiteActions
 */
var AppDispatcher = require('../dispatcher/AppDispatcher'),
  SiteConstants = require('../constants/SiteConstants');

var SiteActions = {
  
  /**
   * @param {object} site
   */
  loadSiteData: function (site) {
    AppDispatcher.handleAPIAction({
      actionType: SiteConstants.LOAD_SITE_DATA,
      site: site,
    });
  },
  
  /**
   * @param {string} route
   */
  updateActiveRoute: function (route) {
    AppDispatcher.handleViewAction({
      actionType: SiteConstants.UPDATE_ACTIVE_ROUTE,
      route: route
    });
  },

  /**
   * @param {object} user
   */
  setActiveUser: function (user) {
    AppDispatcher.handleViewAction({
      actionType: SiteConstants.SET_ACTIVE_USER,
      user: user
    });
  },

  /**
   * @param {string} faviconUrl
   */
  updateFavicon: function (faviconUrl) {
    console.log("SiteActions", SiteActions); 
  }

};

module.exports = SiteActions;