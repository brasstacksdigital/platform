/**
 * Client side js
 */
import React from 'react';
import {Router, Route} from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import JSON2 from 'JSON2';
import SiteApp from './components/site/App';
//SiteApp = React.createFactory(require('./components/site/App')),
import AdminApp from './components/admin/App';
import Settings from './components/admin/Settings';
import Page from './components/admin/Page';

// Snag the initial state that was passed from the server side
// Use JSON2 library to handle circular references in nested components
const initialState = JSON2.retrocycle(JSON2.parse(document.getElementById('initial-state').innerHTML));
const mount = document.getElementById('mnt');

// Render the components, picking up where react left off on the server for sites
if (window.location.pathname.indexOf("admin") === 1) {
  let history = createBrowserHistory()

  const routes = (
    <Route name="admin" path="/admin" component={AdminApp}>
      <Route path="/admin/settings" component={Settings} />
      <Route path="/admin/page/:pageId" component={Page} />
    </Route>
  );

  React.render(<Router history={history}>{routes}</Router>, mount);
} else {
  React.render(SiteApp(initialState), mount)
}
