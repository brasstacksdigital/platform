/*
 * Component Component
 */
var React = require('react'),
  shortid = require('shortid'),
  mui = require('material-ui'),
  IconButton = mui.IconButton,
  AdminActions = require('../actions/AdminActions');

// Export the main Component component
var Component = React.createClass({

  editComponent: function (props) {
    console.log("edit props", props);
  },

  /**
   * @return {object}
   */
  requireComponent: function () {
    var component;
    switch (this.props.component.library) {
      case "rbs":
        component = require('react-bootstrap')[this.props.component.name];
        break;
      case "mui":
        component = require('material-ui')[this.props.component.name];
        break;
      case "scroll":
        component = require('react-scroll')[this.props.component.name];
        break;
      case "bt":
        component = require('./site/' + this.props.component.name);
        break;
      case "base":
        component = this.props.component.name;
        break;
    }
    return component;
  },

  /**
   * @param {array} configs
   * @return {object}
   */
  renderComponents: function (configs) {
    var components = configs.map(function (component_config) {
      return React.createElement(Component, {
        component: component_config
      });
    });
    return components;
  },

  /**
   * @return {object}
   */
  render: function () {

    //console.log("Component.props", this.props.component.name);

    // Check for subcomponents
    var subComponents = this.props.component.components ? this.props.component
      .components : [];
    if (subComponents.length > 0) {
      this.props.component.properties.children = this.renderComponents(
        subComponents);
    }
    // Add key to component properties
    this.props.component.properties.key = shortid.generate();
    // Require component code
    var component = this.requireComponent();
    var RenderedComponent;
    // Add edit button is component is being rendered on admin view and is editable
    if (this.props.component.isEditable && this.props.isAdmin) {
      RenderedComponent = (function () {
        return (
          React.createElement("div", {
              style: {
                position: "realtive"
              }
            },
            React.createElement(component, this.props.component.properties),
            React.createElement(IconButton, {
                title: "Edit " + this.component.name,
                style: {
                  position: "absolute",
                  bottom: 0,
                  right: 0
                },
                onClick: this.editComponent.bind(this, this.props.component
                  .properties)
              },
              React.createElement("i", {
                className: "material-icons"
              }, activeViewIcon)
            )
          )
        );
      }.bind(this))()
    } else {
      RenderedComponent = (function () {
        return (
          React.createElement(component, this.props.component.properties)
        );
      }.bind(this))()
    }
    // Return rendered component
    return RenderedComponent;
  }

});

module.exports = Component;
