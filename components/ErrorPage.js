/*
 * ErrorPage Component
 */
var React = require('react'),
  rbs = require('react-bootstrap'),
  Grid = rbs.Grid,
  Row = rbs.Row,
  Col = rbs.Col,
  mui = require('material-ui'),
  Card = mui.Card,
  CardTitle = mui.CardTitle,
  CardText = mui.CardText;

// Export the main ErrorPage component
var ErrorPage = React.createClass({
	
	/*
   * @return {object}
   */
  render: function () {
    
    var errorText = this.props.error ? this.props.error.message : "Something's up and we'll be sure to fix it right away";
    
    return (
      
      React.createElement(Grid, {},
        React.createElement(CardText, {},
          React.createElement(Row, {},
            React.createElement(Col, {xs: 12, sm: 12, md: 12, lg: 12},
              React.createElement(Card, {
                style: {
                  padding: "72px 32px 32px 32px",
                  textAlign: "center"
                }
              },
                React.createElement("img", {
                  className: "bt-transform-top-center",
                  src: "/BT-ErrorTack.png",
                  alt: "BT Error Tack"
                }),
                React.createElement("h2", {}, "Well that's odd!"),
                React.createElement("h4", {}, errorText),
                React.createElement("hr"),
                React.createElement("div", {className: "text-headline"},
                  React.createElement("span", {}, "Feel free to email us if you need anything in the meantime:"),
                  React.createElement("br"),
                  React.createElement("a", {
                    href: "mailto:hello@brasstacksdigital.com?Subject=Houston%20we%20have%20a%20problem."
                  }, "hello@brasstacksdigital.com")
                )
              )
            )
          )
        )
      )
      
    );
    
  }

});

module.exports = ErrorPage;