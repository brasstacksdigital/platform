/*
 * Login Component
 */
import $ from 'jquery';
import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap';
import {Card, CardTitle, CardText, TextField, RaisedButton, CircularProgress, Snackbar} from 'material-ui';

// Export the main Login component
export default class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activeUser: props.activeUser,
      email: "",
      password: "",
      areValid: {},
      loading: false,
      snackbarMessage: ""
    }
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentWillReceiveProps(newProps, oldProps) {
    this.setState(this.getInitialState(newProps));
  }
  
  handleEmailChange(e) {
    this.setState({
      email: e.target.value
    });
  }
  
  handlePasswordChange(e) {
    this.setState({
      password: e.target.value
    });
  }
  
  login(e) {
    e.preventDefault();
    console.log("e", e);
    // Start spinner
    this.setState({loading: true});
    
    $.ajax({
      type: "POST",
      url: "/login",
      data: {},
      dataType: "json"
    }).done(function (data, textStatus, jqXHR) {
      /*
      SiteActions.setActiveUser(data.user);
      SiteActions.updateActiveRoute("/admin");
      */
    }.bind(this)).fail(function (jqXHR, textStatus, error) {
      // Set snackbar message
      this.setState({
        snackbarMessage: jqXHR.responseText,
        loading: false,
      });
      this.refs.snackbar.show();
    }.bind(this));
  }

  logout(e) {
    e.preventDefault();
    // Start spinner
    this.setState({loading: true});
    
    $.ajax({
      type: "GET",
      url: "/logout",
      dataType: "text"
    }).done(function (data, textStatus, jqXHR) {
      SiteActions.setActiveUser(null);
      // Set snackbar message
      this.setState({
        snackbarMessage: data,
        loading: false
      });
      this.refs.snackbar.show();
    }.bind(this)).fail(function (jqXHR, textStatus, error) {
      // Set snackbar message
      this.setState({
        snackbarMessage: "Oops someything went wrong and we could not log you out",
        loading: false,
      });
      this.refs.snackbar.show();
    }.bind(this));
  }

	/*
   * @return {object}
   */
  render() {
    
    let loginStyle = {
      form: {
        position: "relative",
        margin: "16px 0"
      },
      
      hint: {
        color: "red",
        fontSize: "12px"
      }
    };
    
    return (
      <form 
        style={loginStyle.form}
        method="POST"
        action="/login"
        onSubmit={this.checkForm}
      >
        <Snackbar ref="snackbar"  message={this.state.snackbarMessage} autoHideDuration={3000} />
        <CircularProgress
          className={this.state.loading === true ? "bt-transform-center-center bt-visible" : "bt-transform-center-center bt-hidden"}
          mode="indeterminate"
        />
        <Grid>
          <Card>
            <CardTitle title="Login" subtitle="Please login using your email and password" />
            <CardText>
              <p style={{display: Boolean(this.state.activeUser) ? 'block' : 'none'}}>
                <span>You are already logged in as {this.state.activeUser ? this.state.activeUser.email : ""}. </span>
                <span>To login as another user please </span>
                <a href="/logout" onClick={this.logout}>logout</a>
              </p>
              <Row>
                <Col xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    hintText="Your email"
                    floatingLabelText="Email"
                    fullWidth={true}
                    name="email"
                    value={this.state.email}
                    onChange={this.handleEmailChange}
                    disabled={Boolean(this.state.activeUser)}
                  />
                  <span className={this.state.areValid.email === false ? 'bt-visible' : 'bt-hidden'} style={loginStyle.hint}>
                    Please enter valid email
                  </span>
                </Col>
                <Col xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    hintText="Your password"
                    floatingLabelText="Password"
                    fullWidth={true}
                    name="password"
                    value={this.state.password}
                    onChange={this.handlePasswordChange}
                    disabled={Boolean(this.state.activeUser)}
                  />
                  <span className={this.state.areValid.email === false ? 'bt-visible' : 'bt-hidden'} style={loginStyle.hint}>
                    Please enter password
                  </span>
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={12} md={12} lg={12}>
                  <RaisedButton label="login" primary={true} disabled={Boolean(this.state.activeUser)} style={{float: 'right'}} />
                </Col>
              </Row>
            </CardText>
          </Card>
        </Grid>
      </form>
    );
    
  }

};