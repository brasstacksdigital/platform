/**
 * App Component
 */
import React from 'react';
import Header from './Header';
import mui from 'material-ui';
import JSON2 from 'JSON2';
import AdminActions from '../../actions/AdminActions';
import AdminStore from '../../stores/AdminStore';

const ThemeManager = new mui.Styles.ThemeManager();

/**
 * Retrieve the current admin data from the AdminStore
 */
function getAdminState() {
  return AdminStore.getAdminConfig();
}

// Export the main App component
export default class App extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = JSON2.retrocycle(JSON2.parse(document.getElementById('initial-state').innerHTML));
  }

  /**
   * Get context
   */
  getChildContext() {
    return {
      muiTheme: ThemeManager.getCurrentTheme(),
      clientId: this.state.id,
      hostname: this.state.site.hostname,
    };
  }

  componentDidMount() {
    AdminStore.addChangeListener(this._onChange);
    AdminActions.loadAdminConfigData(this.state);
  }

  componentWillUnmount() {
    AdminStore.removeChangeListener(this._onChange);
  }

  /**
   * @return {object}
   */
  render() {
    return ( 
      <div>
        <Header clientName={this.state.name} />
        <div style={{paddingTop: '48px'}}>
          {this.props.children}
        </div>
      </div>
    );
  }

  /**
   * Event handler for 'change' events coming from the AdminStore
   */
  _onChange() {
    this.setState(getAdminState());
  }

};

App.childContextTypes = {
  muiTheme: React.PropTypes.object,
  clientId: React.PropTypes.string,
  hostname: React.PropTypes.string,
};
