/**
 * Header Component
 */
import React from 'react';
import {IconButton} from 'material-ui';
import {Link} from 'react-router';
import AdminActions from '../../actions/AdminActions';

// Export the main Header component
export default class Header extends React.Component {

  /**
   * @return {object}
   */
  render() {
    return (
      <div
        style={{
          position: 'fixed',
          width: '100%',
          minHeight: '48px',
          padding: '16px',
          boxShadow: '0 1px 2px #aaa',
          zIndex: 1
        }}
      >
        <div style={{position: 'relative', width: '100%'}}>
          <div style={{fontSize: '1.5em'}}>
            Admin Panel - {this.props.clientName}
          </div>
          <div className="bt-transform-center-right">
            <Link to="/admin/settings">
              <IconButton><i className="material-icons">settings</i></IconButton>
            </Link>
          </div>
        </div>
      </div>
    );
  }

};
