/**
 * Integrations Component
 */
import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap'

// Export the main Integrations component
export default class Integrations extends React.Component {

  constructor(props) {
    super(props);
  }

  /**
   * @return {object}
   */
  render() {
    return (
      <Grid className="bt-margin-top bt-margin-bottom">
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            Integrations
          </Col>
        </Row>
      </Grid>
    );
  }

};
