/**
 * Page Component
 */
var React = require('react');

// Export the main Page component
var Page = React.createClass({

  /**
   * @return {object}
   */
  render: function () {

    console.log("Page.props", this.props);

    /*
    var pageComponents = this.props.activeRoute.components.map(function (
      pageComponentConfig) {
      return React.createElement(Component, {
        isAdmin: true,
        component: pageComponentConfig
      });
    });
    */

    return (
      React.createElement("div", {},
        "Page editor"
      )
    );
  }

});

module.exports = Page;
