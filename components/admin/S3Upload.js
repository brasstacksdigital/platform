/*
 * S3Upload Component
 */
var $ = require('jquery'),
  React = require('react'),
  rbs = require('react-bootstrap'),
  Grid = rbs.Grid,
  Row = rbs.Row,
  Col = rbs.Col,
  mui = require('material-ui'),
  TextField = mui.TextField,
  RaisedButton = mui.RaisedButton,
  CircularProgress = mui.CircularProgress,
  Snackbar = mui.Snackbar;

// Export the main S3Upload component
var S3Upload = React.createClass({
  
  /**
   * Get initial component state
   * @param {object} props
   * @return {object}
   */
  getInitialState: function (props) {
    props = props || this.props;
    // Set initial application state using props
    return {
      singleUpload: props.singleUpload,
      images: [],
      loading: false,
      isFileValid: true,
      snackbarMessage: ""
    };
  },
  
  componentWillReceiveProps: function(newProps, oldProps) {
    this.setState(this.getInitialState(newProps));
  },
  
  uploadFile: function (file, signedRequest, url) {
    // Upload file using xhr
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", signedRequest);
    // Set to public read and chache control of s3 images to 1 year
    xhr.setRequestHeader('x-amz-acl', 'public-read', 'x-amz-meta-Cache-Control', 'max-age=31536000');
    xhr.onload = function() {
      if (xhr.status === 200) {
        // Add image to state array
        this.state.images.push({
          name: file.name,
          url: url
        });
        this.setState({
          snackbarMessage: "Your file is ready to be uploaded",
          loading: false
        });
        this.refs.snackbar.show();
        // Clear file input
        $("file-input").val("");
      }
    }.bind(this);
    xhr.onerror = function() {
      this.setState({
        snackbarMessage: "Could not ready file.",
        loading: false
      });
      this.refs.snackbar.show();
    }.bind(this);
    xhr.send(file);
  },
  
  getSignedRequest: function (file) {
    // Start spinner
    this.setState({loading: true});
    
    $.ajax({
      type: "GET",
      url: "/api/uploads/sign_s3?file_name="+file.name+"&file_type="+file.type,
      dataType: "json"
    }).done(function (data, textStatus, jqXHR) {
      this.uploadFile(file, data.signedRequest, data.url);
    }.bind(this)).fail(function (jqXHR, textStatus, error) {
      this.setState({
        snackbarMessage: "Could not get signed URL.",
        loading: false
      });
      this.refs.snackbar.show();
    }.bind(this));
  },
  
  checkFile: function (e) {
    var files = e.target.files;
    var file = files[0];
    if (file === null) {
      this.setState({isFileValid: false});
    }
    else{
      this.getSignedRequest(file);
    }
  },
  
  /*
   * @return {object}
   */
  render: function () {

    // Map all images in state array to preview image elements
    var Images;
    if (this.state.images.length > 0) {
      Images = this.state.images.map(function (image) {
        return (
          React.createElement("img", {
            src: image.url,
            alt: image.name,
            style: {
              width: "200px",
              height: "auto",
              margin: "4px"
            }
          })
        );
      });
    } else {
      Images = "";
    }
    
    var s3UploadStyle = {
      form: {
        position: "relative"
      },
      
      hint: {
        color: "red",
        fontSize: "12px"
      }
    };
    
    return (
      React.createElement("form", {
        style: s3UploadStyle.form,
        method: "POST",
        action: "/api/uploads",
        onSubmit: this.checkForm
      },
        React.createElement(Snackbar, {
          ref: "snackbar",
          message: this.state.snackbarMessage,
          autoHideDuration: 3000
        }),
        React.createElement(CircularProgress, {
          className: this.state.loading === true ? "bt-transform-center-center bt-visible" : "bt-transform-center-center bt-hidden",
          mode: "indeterminate"
        }),
        React.createElement(Grid, {fluid: true},
          React.createElement(Row, {},
            React.createElement(Col, {xs: 12, sm: 12, md: 12, lg: 12},
              React.createElement("p", {
                className: "text-headline",
                style: {
                  display: (this.state.images.length > 0) ? "block" : "none"
                }
              }, "Preview of images to be uploaded")
            )
          ),
          React.createElement(Row, {},
            React.createElement(Col, {xs: 12, sm: 12, md: 12, lg: 12},
              Images
            )
          ),
          React.createElement(Row, {},
            React.createElement(Col, {xs: 12, sm: 12, md: 12, lg: 12},
              React.createElement(TextField, {
                fullWidth: true
              },
                React.createElement("input", {
                  id: "file-input",
                  type: "file",
                  onChange: this.checkFile
                })
              ),
              React.createElement("span", {
                className: this.state.isFileValid === false ? "bt-visible" : "bt-hidden",
                style: s3UploadStyle.hint
              }, "The file was invalid")
            )
          )
        )
      )
    );
    
  }
  
});

module.exports = S3Upload;
