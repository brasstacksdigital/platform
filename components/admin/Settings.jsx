/**
 * Settings Component
 */
import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap'
import {FlatButton} from 'material-ui';
import {instragram as instagramConfig, pinterest as pinterestConfig} from '../../config/integrations';
import AdminStore from '../../stores/AdminStore';

// Export the main Settings component
export default class Settings extends React.Component {

  constructor(props) {
    super(props);
    console.log("props", props);
  }

  /**
   * @return {object}
   */
  render() {
    // Instagram auth url with client id for storing auth
    var instagramAuthUrl =
      "https://api.instagram.com/oauth/authorize/?client_id=" +
      instagramConfig.clientId + "&redirect_uri=" + instagramConfig.redirectUri +
      "?client_id=" + this.clientId + "&response_type=code";

    // Show instagram user or auth button
    var InstagramIntegrationComponent;
    console.log("this.integrations", this.integrations);
    if (true) {
      InstagramIntegrationComponent = (
        <a href={instagramAuthUrl}>
          <FlatButton label="log in with instagram" primary={true} />
        </a>
      )
    } else {
      InstagramIntegrationComponent = (
        <span className="text-title">Logged in as {this.integrations.instagram.user.username}</span>
      )
    }

    return (
      <Grid fluid={true}>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            <h4 style={{padding: '16px 0'}}>Settings</h4>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={3} md={3} lg={3}>
            <span className="text-headline">Instagram API</span>
          </Col>
          <Col xs={12} sm={99} md={9} lg={9}>{InstagramIntegrationComponent}</Col>
        </Row>
      </Grid>
    );
  }

};
