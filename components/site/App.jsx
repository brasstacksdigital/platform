/**
 * App Component
 */
import React from 'react';
import Login from '../Login';
import ErrorPage from '../ErrorPage';
import BTHeader from './BTHeader';
import Component from '../Component';
import mui from 'material-ui';
import SiteActions from '../../actions/SiteActions';
import SiteStore from '../../stores/SiteStore';

const ThemeManager = new mui.Styles.ThemeManager();

/**
 * Retrieve the current site data from the SiteStore
 */
function getSiteState() {
  return {
    site: SiteStore.getSite(),
  };
}

// Export the main App component
export default class App extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = props;
  }

  /**
   * Get context
   */
  getChildContext() {
    return {
      muiTheme: ThemeManager.getCurrentTheme(),
      clientId: this.state.id,
      hostname: this.state.site.hostname,
      storeInfo: this.state.store,
      integrations: this.state.site.integrations
    };
  }

  componentWillReceiveProps(newProps, oldProps) {
    this.setState(this.getInitialState(newProps));
  }

  componentWillMount() {
    // Set the color palette from the site configuration object
    ThemeManager.setPalette(this.state.site.colorPalette);
  }

  componentDidMount() {
    SiteStore.addChangeListener(this._onChange);
    SiteActions.loadSiteData(this.state.site);
  }

  componentWillUnmount() {
    SiteStore.removeChangeListener(this._onChange);
  }

  /**
   * @param {object} activeRoute
   * @return {array}
   */
  getPageComponents(activeRoute) {
    var pageComponents;
    switch (activeRoute.path) {
      case "/login":
        pageComponents = <Login activeUser={this.state.site.activeUser} />
        break;
      case "/error":
        pageComponents = <ErrorPage error={activeRoute.error} />
        break;
      default:
        pageComponents = activeRoute.components.map(pageComponentConfig => <Component component={pageComponentConfig} />);
        break;
    }
    return pageComponents;
  }

  /**
   * @return {object}
   */
  render() {

    // Check the app state to decide which view components to render
    let pageComponents = this.getPageComponents(this.state.site.activeRoute);
    // Don't show normal header in admin panel and adjust padding accordingly
    let pageComponentsPadding = "0px",
      header;
    if (this.state.site.activeRoute.path !== "/admin") {
      header = <BTHeader
        clientName={this.state.name}
        logo={this.state.site.logo}
        routes={this.state.site.routes}
        activeRoute={this.state.site.activeRoute.path}
      />
      pageComponentsPadding = this.state.site.logo.size.height + 'px' || '48px';
    }

    return (
      <div>
        <div>{header}</div>
        <div style={{ paddingTop: pageComponentsPadding /* Clear the fixed header */ }}>{pageComponents}</div>
      </div>
    );
  }

  /**
   * Event handler for 'change' events coming from the SiteStore
   */
  _onChange() {
    this.setState(getSiteState());
  }

};

App.childContextTypes = {
  muiTheme: React.PropTypes.object,
  clientId: React.PropTypes.string,
  hostname: React.PropTypes.string,
  storeInfo: React.PropTypes.object,
  integrations: React.PropTypes.object
};
