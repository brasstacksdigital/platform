/**
 * BTArrowAnimation Component
 */
var $ = require('jquery'),
  React = require('react');

// Export the main BTArrowAnimation component
var BTArrowAnimation = React.createClass({
  
  componentDidMount: function () {
    var start = null;
    var arrow = document.getElementById("arrow");
    
    function step(timestamp) {
      if (!start) start = timestamp;
      var progress = (timestamp - start)/1000,
        pos = arrow.getBoundingClientRect().left,
        vel = 8,
        acc = 9.8;
      arrow.style.left = pos - vel*progress + 0.5*acc*(progress^2) + "px";
      if (progress < 4000) {
        window.requestAnimationFrame(step);
      }
    }
    
    window.requestAnimationFrame(step);
  },
	
	/**
   * @return {object}
   */
	render: function () {
    
    return (
      React.createElement("div", {
        style: {
          position: "relative",
          width: "500px",
          height: "5000px"
        }
      },
        React.createElement("div", {
          id: "arrow",
          style: {
            position: "absolute",
            top: "480px",
            left: "480px",
            width: "100px",
            height: "100px",
            backgroundColor: "#000"
          }
        })
      )
    );
	}
	
});

module.exports = BTArrowAnimation;