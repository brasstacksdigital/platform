/*
 * BTEmailForm Component
 */
var $ = require('jquery'),
  React = require('react'),
  validator = require('validator'),
  configEmail = require('../../config/email'),
  rbs = require('react-bootstrap'),
  Grid = rbs.Grid,
  Row = rbs.Row,
  Col = rbs.Col,
  mui = require('material-ui'),
  TextField = mui.TextField,
  RaisedButton = mui.RaisedButton,
  CircularProgress = mui.CircularProgress,
  Snackbar = mui.Snackbar;

// Export the main BTEmailForm component
var BTEmailForm = React.createClass({
  
  getInitialState: function(){
    // we don't want to validate the input until the user starts typing
    return {
      name: "",
      email: "",
      company: "",
      subject: "Request a Free Consultation",
      message: "",
      areValid: {},
      loading: false,
      snackbarMessage: ""
    };
  },
  
  handleNameChange: function (e) {
    this.setState({
      name: e.target.value
    });
  },
  
  handleEmailChange: function (e) {
    this.setState({
      email: e.target.value
    });
  },
  
  handleCompanyChange: function (e) {
    this.setState({
      company: e.target.value
    });
  },
  
  handleSubjectChange: function (e) {
    this.setState({
      subject: e.target.value
    });
  },
  
  handleMessageChange: function (e) {
    this.setState({
      message: e.target.value
    });
  },
  
  sendEmail: function (state) {
    // Start spinner
    this.setState({loading: true});
    
    $.ajax({
      type: "POST",
      url: configEmail.apiEndpoint,
      data: state,
      dataType: "text"
    }).done(function (data, textStatus, jqXHR) {
      // Clear form and set snackbar message
      this.setState({
        name: "",
        email: "",
        company: "",
        subject: "",
        message: "",
        areValid: {},
        loading: false,
        snackbarMessage: configEmail.successMessage
      });
      this.refs.snackbar.show();
    }.bind(this)).fail(function (jqXHR, textStatus, error) {
      // Set snackbar message
      this.setState({
        snackbarMessage: jqXHR.responseText,
        loading: false,
      });
      this.refs.snackbar.show();
    }.bind(this));
  },
  
  validate: function (state) {
    return {
      name: validator.isLength(state.name, 1, 140),
      email: validator.isEmail(state.email),
      company: validator.isLength(state.company, 1, 140),
      subject: validator.isLength(state.subject, 1, 140),
      message: validator.isLength(state.message, 1, 140)
    };
  },
  
  checkForm: function (e) {
    e.preventDefault();
    var areValid = this.validate(this.state);
    
    this.setState({
      areValid: areValid
    });
    
    for(var input in areValid) {
      if(!areValid[input]) return false;
    }
    
    this.sendEmail(this.state);
  },
  
  /*
   * @return {object}
   */
  render: function () {
    
    var btEmailFormStyle = {
      form: {
        position: "relative"
      },
      
      hint: {
        color: "red",
        fontSize: "12px"
      }
    };
    
    return (
      React.createElement("form", {
        style: btEmailFormStyle.form,
        method: "POST",
        action: configEmail.apiEndpoint,
        onSubmit: this.checkForm
      },
        React.createElement(Snackbar, {
          ref: "snackbar",
          message: this.state.snackbarMessage,
          autoHideDuration: 3000
        }),
        React.createElement(CircularProgress, {
          className: this.state.loading === true ? "bt-transform-center-center bt-visible" : "bt-transform-center-center bt-hidden",
          mode: "indeterminate"
        }),
        React.createElement(Grid, {},
          React.createElement(Row, {},
            React.createElement(Col, {xs: 12, sm: 12, md: 12, lg: 12, style: {marginBottom: "16px"}},
              React.createElement("h4", {}, "Reach out.")
            )
          ),
          React.createElement(Row, {},
            React.createElement(Col, {xs: 12, sm: 6, md: 6, lg: 6},
              React.createElement(TextField, {
                hintText: "Your name",
                floatingLabelText: "Name",
                fullWidth: true,
                name: "name",
                value: this.state.name,
                onChange: this.handleNameChange
              }),
              React.createElement("span", {
                className: this.state.areValid.name === false ? "bt-visible" : "bt-hidden",
                style: btEmailFormStyle.hint
              }, configEmail.errorMessaging.name)
            ),
            React.createElement(Col, {xs: 12, sm: 6, md: 6, lg: 6},
              React.createElement(TextField, {
                hintText: "Your email",
                floatingLabelText: "Email",
                fullWidth: true,
                name: "email",
                value: this.state.email,
                onChange: this.handleEmailChange
              }),
              React.createElement("span", {
                className: this.state.areValid.email === false ? "bt-visible" : "bt-hidden",
                style: btEmailFormStyle.hint
              }, configEmail.errorMessaging.email)
            )
          ),
          React.createElement(Row, {},
            React.createElement(Col, {xs: 12, sm: 6, md: 6, lg: 6},
              React.createElement(TextField, {
                hintText: "Your company",
                floatingLabelText: "Company",
                fullWidth: true,
                name: "company",
                value: this.state.company,
                onChange: this.handleCompanyChange
              }),
              React.createElement("span", {
                className: this.state.areValid.company === false ? "bt-visible" : "bt-hidden",
                style: btEmailFormStyle.hint
              }, configEmail.errorMessaging.company)
            ),
            React.createElement(Col, {xs: 12, sm: 6, md: 6, lg: 6,},
              React.createElement(TextField, {
                hintText: "Message subject",
                floatingLabelText: "Subject",
                fullWidth: true,
                name: "subject",
                value: this.state.subject,
                onChange: this.handleSubjectChange
              }),
              React.createElement("span", {
                className: this.state.areValid.subject === false ? "bt-visible" : "bt-hidden",
                style: btEmailFormStyle.hint
              }, configEmail.errorMessaging.subject)
            )
          ),
          React.createElement(Row, {},
            React.createElement(Col, {xs: 12, sm: 12, md: 12, lg: 12},
              React.createElement(TextField, {
                hintText: "Your message to us...",
                floatingLabelText: "Message",
                multiLine: true,
                fullWidth: true,
                name: "message",
                value: this.state.message,
                onChange: this.handleMessageChange
              }),
              React.createElement("span", {
                className: this.state.areValid.message === false ? "bt-visible" : "bt-hidden",
                style: btEmailFormStyle.hint
              }, configEmail.errorMessaging.message)
            )
          ),
          React.createElement(Row, {},
            React.createElement(Col, {xs: 12, sm: 12, md: 12, lg: 12,
		style: {
			"marginTop": "16px"
		}},
              React.createElement(RaisedButton, {
                label: "send",
                primary: true,
                style: {float: "right"}
              })
            )
          )
        )
      )
    );
    
  }
  
});

module.exports = BTEmailForm;
