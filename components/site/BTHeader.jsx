/**
 * BTHeader Component
 */
import React from 'react';
import Link from './Link';
import {Link as ScrollLink} from 'react-scroll';
import MediaQuery from 'react-responsive';
import BTShadeOverlay from './BTShadeOverlay';
import {IconButton} from 'material-ui';

// Export the main BTHeader component
export default class BTHeader extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.color = context.muiTheme.palette.textColor;
    this.accentColor = context.muiTheme.palette.accent1Color;
    this.toggleShade = this.toggleShade.bind(this);
  }

  toggleShade() {
    this.refs.shade.toggle();
  }

  mapRoutesToList(listStyle, activeRoute, doesToggleShade) {
    let listItems = this.props.routes.map((route, index, array) => {
      // Don't return item for / route since that is mapped to logo
      if (route.path === "/") {
        return null;
      }
      let link;
      // Check if adding seperator between list items
      let seperator = null
      if (array.length - 1 !== index && !doesToggleShade) {
        seperator = <span> | </span>
      }
      if (route.inPage) {
        link = (
          <div>
            <ScrollLink 
              key={index} to={route.to} smooth offset={-62} duration={500}
              onClick={doesToggleShade ? this.toggleShade : null}
            >
              {route.title}
            </ScrollLink>
            {seperator}
          </div>
        );
      } else {
        link = (
          <div>
            <Link 
              key={index} href={route.path} route={route.path} 
              color={(route.path === activeRoute) ? null : this.color}
              onClick={doesToggleShade ? this.toggleShade : null}
            >
              {route.title}
            </Link>
            {seperator}
          </div>
        );
      }
      return (
        <li key={route.key} style={listStyle}>{link}</li>
      );
    });
    return listItems;
  }

  /**
   * @return {object}
   */
  render() {
    // if route prop exists...
    let activeRoute = this.props.activeRoute,
      navListItems, shadeListItems;
    if (this.props.routes) {
      // Build list of each nav item for header
      navListItems = this.mapRoutesToList({
          display: "inline-block",
          padding: "4px"
        },
        activeRoute, false);
      // Build list of each nav item for shade
      shadeListItems = this.mapRoutesToList({
          display: "block",
          width: "100%",
          margin: "24px 0",
          fontSize: "2.5rem",
        },
        activeRoute, true);
    }

    // Create header image if logo url available
    let logoWidth = this.props.logo.size.width + 'px' || 'auto';
    let logoHeight = this.props.logo.size.height + 'px' || '48';
    let logo = this.props.logo.url ? 
      <img style={{ margin: '8px 4px 0 8px', width: logoWidth, height: logoHeight }} src={this.props.logo.url} alt="logo image" /> :
      <div style={{ marginLeft: '4px' }} className="text-headline">{this.props.clientName}</div>;

    return (
      <div
        style={{
          position: 'fixed',
          width: '100%',
          minHeight: '48px',
          backgroundColor: '#fff',
          color: '#000',
          boxShadow: '0 1px 2px #aaa',
          zIndex: 1
        }}
      >
        <Link href="/" route="/">{logo}</Link>
        <div style={{ display: 'inline-block' }}>
          <MediaQuery minWidth={376} values={{ deviceWidth: 1600 }}>
            <div
              style={{
                display: 'inline-block',
                marginBottom: '8px',
                verticalAlign: 'super',
                color: this.accentColor
              }}
            >
              <h5>19th-20th-21st centuries<br />6,500 sq ft</h5>
            </div>
          </MediaQuery>
        </div>
        <MediaQuery minWidth={769} values={{ deviceWidth: 1600 }}>
          <ul className="bt-transform-center-right" style={{ right: '8px' }}>{navListItems}</ul>
        </MediaQuery>
        <MediaQuery maxWidth={768} values={{ deviceWidth: 1600 }}>
          <IconButton className="bt-transform-center-right" onClick={this.toggleShade}>
            <i className="material-icons">menu</i>
          </IconButton>
          <BTShadeOverlay ref="shade">
            <ul>{shadeListItems}</ul>
          </BTShadeOverlay>
        </MediaQuery>
      </div>
    );
  }

};

BTHeader.contextTypes = {
  muiTheme: React.PropTypes.object
};