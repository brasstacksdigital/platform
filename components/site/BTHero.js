/**
 * BTHero Component
 */
var React = require('react'),
  rbs = require('react-bootstrap'),
  Grid = rbs.Grid,
  Row = rbs.Row,
  Col = rbs.Col;

// Export the main BTHero component
var BTHero = React.createClass({
  
  // Needed to get color theme set to all children of App.js
  contextTypes: {
    muiTheme: React.PropTypes.object
  },
	
	/**
   * @return {object}
   */
  render: function () {
    
    // Map all taglines in props to h1 or h2 elements
    var el = this.props.hero ? "h1" : "h2";
    var taglines = this.props.taglines;
    var taglineComponents = "";
    if (taglines) {
      taglineComponents = taglines.map(function (tagline, index) {
        // Don't add padding to last element
        var bottomPadding = (index === taglines.length - 1) ? 0 : "32px";
        return React.createElement(el, {
          style: {
            paddingBottom: bottomPadding
          }
        },
          tagline,
          React.createElement("br")
        );
      });
    }
    
    var btHeroStyle = {
      cont: {
        marginBottom: "24px",
        padding: this.props.hero ? "72px 24px" : "24px",
        backgroundColor: this.context.muiTheme.palette.primary1Color,
        textAlign: this.props.hero ? "center" : "left"
      }
    };
    
    return (
      React.createElement(Grid, {
        fluid: true,
        className: this.props.className || ""
      },
        React.createElement(Row, {},
          React.createElement(Col, {xs:12, sm:12, md:12, lg:12, style: btHeroStyle.cont}, taglineComponents)
        )
      )
    );
	}
	
});

module.exports = BTHero;