/**
 * BTInfoHero Component
 */
import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap'

// Export the main BTInfoHero component
export default class BTInfoHero extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.hours = context.storeInfo.hours;
    this.address = context.storeInfo.address;
    this.phoneNumber = context.storeInfo.phoneNumber;
    this.backgroundColor = context.muiTheme.palette.primary1Color;
  }

  // Check is store is open based on hours config
  checkTime() {
    let date = new Date(),
      day = date.getDay(),
      hours = date.getHours(),
      minutes = date.getMinutes(),
      currentTime = hours.toString() + minutes.toString(),
      currentHours = this.hours.real[day];
    return (currentTime >= currentHours[0] && currentTime <= currentHours[1]) ? true : false;
  }

  /**
   * @return {object}
   */
  render() {
    // Check if store is open
    let open = this.checkTime();

    return (
      <div
        className="bt-background-img-cover"
        style={{ background: 'url("' + this.props.backgroundImg + '") no-repeat center center' }}
      >
        <Grid>
          <Row>
            <Col 
              xs={8} sm={4} md={4} lg={3}
              style={{ 
                padding: '16px',
                backgroundColor: this.backgroundColor,
                color: '#fff',
                textAlign: 'center'
              }}
            >
              <h4>{open ? 'Open Now' : 'Now Closed'}</h4>
              <div className="text-title" style={{ lineHeight: '30px' }}>
                {this.hours.display[0]} <br /> {this.hours.display[1]}
              </div>
              <hr />
              <a
                className="text-title"
                style={{lineHeight: '30px', color: '#fff'}}
                href={'http://maps.google.com/?q=' + this.address.display.toString()}
                target="_blank"
              >
                <div>
                  {this.address.display[0]} <br /> {this.address.display[1]} <br /> {this.address.display[2]}
                </div>
              </a>
              <hr />
              <div className="text-title" style={{ lineHeight: '30px' }}>
                {this.phoneNumber.display}
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }

};

BTInfoHero.contextTypes = {
  muiTheme: React.PropTypes.object,
  storeInfo: React.PropTypes.object
};
