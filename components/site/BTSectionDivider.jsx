/**
 * BTSectionDivder Component
 */
import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap'

// Export the main BTSectionDivder component
export default class BTSectionDivder extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.backgroundColor = context.muiTheme.palette.backgroundColor;
    this.headerColor = context.muiTheme.palette.primary1Color
    this.color = context.muiTheme.palette.accent1Color;
    this.borderColor = context.muiTheme.palette.textColor
  }

  /**
   * @return {object}
   */
  render() {
    return (
      <Grid className="bt-margin-top bt-margin-bottom">
        <Row>
          <Col xs={12} sm={12} md={12} lg={12} style={{ position: 'relative', textAlign: 'center'}}>
            <div
              className="bt-transform-center-center"
              style={{
                position: 'absolute',
                width: '90%',
                borderBottom: '2px solid ' + this.headerColor
              }}
            ></div>
            <h3
              style={{
                position: 'relative',
                display: 'inline-block',
                padding: '0 4px',
                backgroundColor: this.backgroundColor,
                color: this.headerColor
              }}
            >
              {this.props.title}
            </h3>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12} style={{ color: this.color, textAlign: 'center'}}>
            <p>{this.props.subTitle}</p>
          </Col>
        </Row>
      </Grid>
    );
  }

};

BTSectionDivder.contextTypes = {
  muiTheme: React.PropTypes.object,
};
