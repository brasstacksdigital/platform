/**
 * BTShadeOverlay Component
 */
import React from 'react';
import {IconButton} from 'material-ui';

// Export the main BTShadeOverlay component
export default class BTShadeOverlay extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = { active: false };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({ active: !this.state.active });
  }
	
	/**
   * @return {object}
   */
	render() {   
    return (
      <div
        style={{
          position: 'fixed',
          top: this.state.active ? 0 : '-100vh',
          left: 0,
          width: '100%',
          height: '100%',
          backgroundColor: '#fffdfb',
          zIndex: 100,
          transition: 'top 0.25s ease-in',
          WebkitTransition: 'top 0.25s ease-in',
          msTransition: 'top 0.25s ease-in'
        }}
      >
        <IconButton
          style={{
            position: 'absolute',
            top: 0,
            right: 0
          }}
          onClick={this.toggle}
        >
          <i className="material-icons">close</i>
        </IconButton>
        <div className="bt-transform-center-center" style={{ width: '100%', textAlign: 'center' }}>
          {this.props.children}
        </div>
      </div>
    );
	}
	
};
