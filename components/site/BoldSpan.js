/**
 * BoldSpan Component
 */
var React = require('react');

// Export the main BoldSpan component
var BoldSpan = React.createClass({
  
  // Needed to get mui color theme
  contextTypes: {
    muiTheme: React.PropTypes.object
  },
	
	/**
   * @return {object}
   */
	render: function () {
    return (
      React.createElement("span", {
        style: {
          fontWeight: "bold",
          borderBottom: "1px solid " + this.context.muiTheme.palette.primary1Color
        }
      }, this.props.children)
    );
	}
	
});

module.exports = BoldSpan;