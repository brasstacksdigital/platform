/**
 * EmailSignUp Component
 */
import $ from 'jquery';
import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap'
import {RaisedButton, FlatButton, Card} from 'material-ui';

// Export the main EmailSignUp component
export default class EmailSignUp extends React.Component {

  constructor(props, context) {
    super(props);
    this.state = { shown: false, dismissed: false, position: {} };
    this.backgroundColor = context.muiTheme.palette.primary1Color;
    this.color = context.muiTheme.palette.accent1Color;
    this.showEmailSignup = this.showEmailSignup.bind(this);
    this.dismissSignUp = this.dismissSignUp.bind(this);
  }

  componentDidMount() {
    if (!this.state.shown) {
      window.setTimeout(() => {
        this.showEmailSignup();
      }, 5000)
    }
  }

  showEmailSignup() {
    this.setState({ shown: true, position: { position: 'fixed', top: '100px', width: '100%' } });
  }

  dismissSignUp() {
    this.setState({ dismissed: true, position: {} });
  }

  /**
   * @return {object}
   */
  render() {
    // Only show no thanks button when popping up
    var noThanks = null;
    if (!this.state.dismissed) {
      noThanks = <FlatButton onClick={this.dismissSignUp} label="No Thanks" />;
    }

    return (
      <Grid style={this.state.position} className="bt-margin-top bt-margin-bottom bt-padding-bottom">
        <Card style={{ maxWidth: '550px', margin: '0 auto', padding: '16px', zIndex: '10' }}>
          <Row>
            <Col xs={12} sm={12} md={12} lg={12}>
              <h4 style={{ margin: '0 auto', padding: '0 4px', backgroundColor: this.backgroundColor, color: '#fff', textAlign: 'center' }}>
                {this.props.title}
              </h4>
              <p style={{ width: '100%', margin: '8px auto', color: this.color, textAlign: 'center' }}>
                {this.props.subtitle}
              </p>
            </Col>
          </Row>
          <Row>
            <Col style={{ textAlign: 'center' }} xs={12} sm={12} md={12} lg={12}>
              <input style={{ padding: '8px', borderRadius: '3px' }} type="email" name="email" placeholder="Your email" />
              <span style={{ display: 'inline-block', margin: '12px 0', padding: '0 8px' }}>
                <RaisedButton label="Sign Up" primary />
              </span>
              {noThanks}
            </Col>
          </Row>
        </Card>
      </Grid>
    );
  }

};

EmailSignUp.contextTypes = {
  muiTheme: React.PropTypes.object,
  integrations: React.PropTypes.object
};
