/**
 * InstagramGallery Component
 */
import $ from 'jquery';
import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap'
import {CircularProgress} from 'material-ui';

// Export the main InstagramGallery component
export default class InstagramGallery extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      imageData: null
    };
    this.accessToken = context.integrations.instagram.accessToken
    this.userId = context.integrations.instagram.user.id;
  }

  getFeed() {
    $.ajax({
        type: 'GET',
        url: 'https://api.instagram.com/v1/users/' + this.userId + '/media/recent',
        data: {
          access_token: this.accessToken,
          count: 12
        },
        dataType: 'jsonp'
      })
      .done((data, textStatus, jqXHR) => {
        this.setState({
          imageData: data.data
        });
      })
      .fail((jqXHR, textStatus, error) => {});
  }

  componentDidMount() {
    if (!this.state.imageData) {
      this.getFeed();
    }
  }

  /**
   * @return {object}
   */
  render() {
    let imageElements;
    if (this.state.imageData) {
      imageElements = this.state.imageData.map((imageConfig, index) => {
        return (
          <Col style={{ padding: '4px' }} xs={6} sm={6} md={4} lg={3} key={index}>
            <a href={imageConfig.link} target="_blank">
              <img 
                src={imageConfig.images.standard_resolution.url}
                style={{
                  width: '100%',
                  marginBottom: '4px'
                }}
              />
            </a>
          </Col>
        );
      });
    } else {
      imageElements = <CircularProgress 
        className={!this.state.loaded ? 'bt-transform-center-center bt-visible' : 'bt-transform-center-center bt-hidden'}
        mode="indeterminate" 
      />
    }

    return (
      <Grid className="bt-margin-top bt-margin-bottom">
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            <Row style={{
              position: 'relative',
              minHeight: '150px',
              textAlign: 'center'
            }}
            >
              {imageElements}
            </Row>
          </Col>
        </Row>
      </Grid>
    );
  }

};

InstagramGallery.contextTypes = {
  integrations: React.PropTypes.object
};
