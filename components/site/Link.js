/**
 * Link Component
 */
var React = require('react'),
  SiteActions = require('../../actions/SiteActions');

// Export the main Link component
var Link = React.createClass({
  
  // Needed to get mui color theme
  contextTypes: {
    muiTheme: React.PropTypes.object
  },
	
	handleClick: function (event) {
	  event.preventDefault();
	  // Normalize click events from non a tags
	  if (event.target.tagName !== "A") {
	    event.target = event.target.parentNode;
	  }
    SiteActions.updateActiveRoute(event.target.dataset.route);
    // Call onClick prop if defined
    if (this.props.onClick) {
      this.props.onClick();
    }
  },
	
	/**
   * @return {object}
   */
	render: function () {
    
    //console.log("Link.props", this.props);
    
    return (
      React.createElement("a", {
        style: {
          display: "inline-block",
          color: this.props.color || this.context.muiTheme.palette.textColor
        },
        href: this.props.href,
        "data-route": this.props.route,
        onClick: this.handleClick
      }, this.props.children)
    );
	}
	
});

module.exports = Link;