/**
 * NAFooter Component
 */
import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap'
import EmailSignUp from './EmailSignUp';

// Export the main NAFooter component
export default class NAFooter extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.hours = context.storeInfo.hours.display;
    this.address = context.storeInfo.address.display;
    this.phoneNumber = context.storeInfo.phoneNumber.display;
    this.backgroundColor = context.muiTheme.palette.primary1Color;
    this.color = context.muiTheme.palette.textColor;
  }

  /**
   * @return {object}
   */
  render() {
    let marginBottom = { marginBottom: '8px' };
    return (
      <Grid 
        fluid={true}
        style={{
          position: 'relative',
          backgroundColor: this.backgroundColor,
          color: '#fff',
          zIndex: -1 /* Hide footer behind any popups */
        }}>
        <Grid>
          <Row>
            <Col xs={12} sm={6} md={6} lg={6} style={{ padding: '16px 48px', textAlign: 'center'}}>
              {this.props.description}
            </Col>
            <Col xs={12} sm={6} md={6} lg={6} style={{ padding: '16px 48px', textAlign: 'center'}}>
              <div style={marginBottom}>
                {this.hours[0] + ' and ' + this.hours[1]}
              </div>
              <div style={marginBottom}>
                {this.address[0] + ' ' + this.address[1]} <br /> {this.address[2]}
              </div>
              <div style={marginBottom}>
                {this.phoneNumber}
              </div>
            </Col>
          </Row>
        </Grid>
      </Grid>
    );
  }

};

NAFooter.contextTypes = {
  muiTheme: React.PropTypes.object,
  storeInfo: React.PropTypes.object
};
