/**
 * InstagramGallery Component
 */
import $ from 'jquery';
import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap'

// Export the main InstagramGallery component
export default class InstagramGallery extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.link = context.integrations.pinterest.link;
  }

  /**
   * @return {object}
   */
  render() {
    return (
      <Grid className="bt-margin-top bt-margin-bottom">
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            <a href={this.link}>
              <img 
                style={{ width: '100%', margin: '0 auto', borderRadius: '4px' }}
                src="http://p-fst1.pixstatic.com/55d611e3697ab0755400112c/_w.540_s.fit_/headboard-4.png" />
            </a>
          </Col>
        </Row>
      </Grid>
    );
  }

};

InstagramGallery.contextTypes = {
  integrations: React.PropTypes.object
};
