/* Set env variables */
var mongo_user = process.env.MONGO_USER,
	mongo_pass = process.env.MONGO_PASS,
	mongo_host = process.env.MONGO_HOST,
	mongo_port = process.env.MONGO_PORT,
	mongo_path = process.env.MONGO_PATH;

module.exports = "mongodb://" + mongo_user + ":" + mongo_pass + "@" + mongo_host + ":" + mongo_port + "/" + mongo_path;