module.exports = {
  apiEndpoint: "/api/emails",
  email: process.env.SMTP_EMAIL_USER,
  password: process.env.SMTP_EMAIL_PASS,
  confirmationSubject: "Thanks for reaching out to Brass Tacks Digital",
  confirmationText: "Good news - we’ve received your message! And since we’re punctual people it shouldn’t be long before we get back to you.\n\nThanks,\nBrass Tacks Digital.\n\nJessica & Trevor Plassman\nhello@brasstacksdigital.com\nbrasstacksdigital.com",
  confirmationHtml: "<p>Good news - we&rsquo;ve received your message! And since we&rsquo;re punctual people it shouldn&rsquo;t be long before we get back to you.</p><p>Thanks,<br>Brass Tacks Digital</p><p>Jessica & Trevor Plassman<br><a href='mailto:hello@brasstacksdigital.com'>hello@brasstacksdigital.com</a><br><a href='brasstackdigital.com'>brasstacksdigital.com</a></p>",
  errorMessaging: {
    name: "Don't forget your name (140 characters or less)",
    email: "Hmm...seems your email isn't right",
    company: "Add your company (140 characters or less)",
    subject: "Let us know what you need (140 characters or less)",
    message: "Give us a tidbit of information (700 characters or less)",
    sendingMessage: "Oops. There was an problem sending the message through our server."
  },
  successMessage: "Thanks for reaching out!"
};
