/**
 * config/googleServiceAccount.js
 * Exports Google Service Account api configuration object
 */

module.exports = {
  clientEmail: process.env.GOOGLE_SERVICE_ACCOUNT_CLIENT_EMAIL,
  privateKey: process.env.GOOGLE_SERVICE_ACCOUNT_PRIVATE_KEY
};