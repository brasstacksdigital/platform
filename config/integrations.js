/**
 * config/integrations.js
 * Exports third party integration api configuration object
 * Make client ID and redirect url available in client
 */

export const instragram = {
  clientId: process.env.INSTAGRAM_CLIENT_ID || "809234ed305f443091055ac0b60d8bf9",
  clientSecret: process.env.INSTAGRAM_CLIENT_SECRET,
  redirectUri: process.env.INSTAGRAM_REDIRECT_URI || "http://localhost:8000/api/integrations/instagram/redirect"
}

export const pinterest = {
  clientId: process.env.PINTEREST_CLIENT_ID || "4792842424877588558",
  clientSecret: process.env.PINTEREST_CLIENT_SECRET,
  redirectUri: process.env.PINTEREST_REDIRECT_URI || "http://localhost:8000/api/integrations/pinterest/redirect"
}