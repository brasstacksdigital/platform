/**
 * SiteConstants
 */
var keyMirror = require('react/lib/keyMirror');

// Define action constants
module.exports = keyMirror({
	LOAD_ADMIN_CONFIG_DATA: null,
	UPDATE_ACTIVE_VIEW: null
});