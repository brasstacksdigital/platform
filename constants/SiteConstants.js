/**
 * SiteConstants
 */
var keyMirror = require('react/lib/keyMirror');

// Define action constants
module.exports = keyMirror({
  LOAD_SITE_DATA: null,  // Get initial site data from server
  UPDATE_ACTIVE_ROUTE: null,  // Updates active route for site
  SET_ACTIVE_USER: null  // Set user auth status in app state
});