/**
 * AppDispatcher
 */
var Dispatcher = require('flux').Dispatcher;

// Create dispatcher instance
var AppDispatcher = new Dispatcher();

// Convenience method to handle api dispatch requests
AppDispatcher.handleAPIAction = function (action) {
  this.dispatch({
    source: 'API_ACTION',
    action: action
  });
}

// Convenience method to handle view dispatch requests
AppDispatcher.handleViewAction = function (action) {
  this.dispatch({
    source: 'VIEW_ACTION',
    action: action
  });
}

module.exports = AppDispatcher;