/**
 * Client Model
 */
 
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  Route = require('./route'),
  User = require('./user');

// Define the schema for client model
var clientSchema = new Schema({
	
  name: String,
  description: String,
  updated: {
    type: Date,
    default: Date.now
  },
  site: {
    hostname: String,
    favicon: String,
    fontFamily: String,
    logo: {
      url: String,
      size: {
        width: Number,
        height: Number
      }
    },
    colorPalette: {},
    routes: [{ type: Schema.ObjectId, ref: 'Route' }],
    integrations: {
      instagram: {
        accessToken: String,
        user: {
          id: Number,
          username: String
        }
      },
      pinterest: {
        accessToken: String,
        user: {
          id: Number,
          username: String
        },
        link: String
      },
      googleAnalytics: {
        id: String
      }
    },
    users: [{ type: Schema.ObjectId, ref: 'User' }],
  }
	
}, {strict: true, minimize: false});

// Create the model for client and expose it to our app
module.exports = mongoose.model('Client', clientSchema);