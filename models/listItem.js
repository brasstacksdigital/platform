/**
 * ListItem Model
 */
 
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  Client = require('./client');

// Define the schema for list item model
var listItemSchema = new Schema({

  clientID: { type: Schema.ObjectId, ref: 'Client' },
  name: String,
  properties: {}
  
}, {strict: true, minimize: false});

// Create the model for list item and expose it to our app
module.exports = mongoose.model('ListItem', listItemSchema);