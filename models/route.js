/**
 * Route Model
 */
 
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  Client = require('./client');

// Define the schema for route model
var routeSchema = new Schema({

  clientID: { type: Schema.ObjectId, ref: 'Client' },
  inPage: Boolean,
  to: String,
  path: String,
  title: String,
  description: String,
  components: [
    {
      library: String,
      name: String,
      properties: {
        type: Schema.Types.Mixed,
        default: {
          empty: true
        }
      },
      isEditable: Boolean,
      components: []
    }
  ]
  
}, {strict: true, minimize: false});

// Create the model for route and expose it to our app
module.exports = mongoose.model('Route', routeSchema);