/**
 * User schema
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  bcrypt = require('bcrypt-nodejs'),
  Client = require('./client');

// Define the schema for user model
var userSchema = new Schema({

  clientID: { type: Schema.ObjectId, ref: 'Client' },
  local: {
    email: String,
    password: String,
    name: {
      first: String,
      last: String
    },
    isAdmin: {
      type: Boolean,
      default: false
    },
    token: String
  },
  facebook: {
    id: String,
    token: String,
    email: String,
    name: String
  },
  twitter: {
    id: String,
    token: String,
    displayName: String,
    username: String
  },
  google: {
    id: String,
    token: String,
    email: String,
    name: String
  }

});

// Methods ======================
// Generating a hash
userSchema.methods.generateHash = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// Checking if password is valid
userSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.local.password);
};

// Create the model for user and expose it to our app
module.exports = mongoose.model('User', userSchema);