/**
 * routes//admin/index.js
 */
var JSON2 = require('JSON2'),
  utilities = require('../../utils/utilities');

module.exports = function (Client) {
  var router = require('express')
    .Router();

  router.get('/:path?', function (req, res) {
    // Redirect admin requests to login if not authenticated
    if (!req.isAuthenticated()) {
      res.redirect("/login");
      return;
    }
    var host = req.get("X-Development") ? "development" : req.headers.host;
    utilities.fetchClientData(Client, host, function (clientData) {
      // Convert Mongoose object to regular JSON object before render
      clientData = clientData.toObject();
      // Add variables specific to app instance that are not saved in the db
      // Add string id for use in client api requests
      clientData.id = clientData._id.toString();
      // If authenticated set user object
      if (req.isAuthenticated()) {
        clientData.site.activeUser = req.session.passport.user;
      } else {
        clientData.site.activeUser = null;
      }
      // Render our 'app' template with just inital data
      res.render('app', {
        state: JSON2.stringify(JSON2.decycle(clientData)) // Use JSON2 library to handle circular references in nested components
      });
    })
  });

  return router;
};
