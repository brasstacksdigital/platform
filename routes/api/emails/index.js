/**
 * routes/api/emails/index.js
 */

var validator = require('validator'),
  nodemailer = require("nodemailer"),
  configEmail = require('../../../config/email');

// Transport object for sending emails
var smtpTransport = nodemailer.createTransport("SMTP", {
  service: "Gmail",
  auth: {
    user: configEmail.email,
    pass: configEmail.password
  }
});

module.exports = function() {
  var router = require('express').Router();
  
  // Send email from client side form
  router.post('/', function (req, res) {
    // Validate form data
    if (!validator.isLength(req.body.name, 1, 140)) {
      res.status(500).send(configEmail.errorMessaging.name);
      return;
    } else if (!validator.isEmail(req.body.email)) {
      res.status(500).send(configEmail.errorMessaging.email);
      return;
    } else if (!validator.isLength(req.body.company, 1, 140)) {
      res.status(500).send(configEmail.errorMessaging.company);
      return;
    } else if (!validator.isLength(req.body.subject, 1, 140)) {
      res.status(500).send(configEmail.errorMessaging.subject);
      return;
    } else if (!validator.isLength(req.body.message, 1, 140)) {
      res.status(500).send(configEmail.errorMessaging.message);
      return;
    }
    
    var extraInformation = "\n\nName: " + req.body.name + "\nEmail: " + req.body.email + "\nCompany: " + req.body.company;
    
    // Send message to hello@brasstacksdigital.com
    var initialMailOptions = {
      from: req.body.email,
      to: configEmail.email,
      subject: req.body.subject,
      text: req.body.message.concat(extraInformation)
    };
    smtpTransport.sendMail(initialMailOptions, function (error, response){
      if (error) {
        console.log("Confirmation message error: " + error);
        res.status(500).send(configEmail.errorMessaging.sendingMessage);
      } else {
        console.log("Initial message sent: " + response.message);
        res.status(200).send(configEmail.successMessage);
      }
    });
    
    // Send confirmation message to initital sender
    var confirmationMailOptions = {
      from: configEmail.email,
      to: req.body.email,
      subject: configEmail.confirmationSubject,
      text: configEmail.confirmationText,
      html: configEmail.confirmationHtml
    };
    smtpTransport.sendMail(confirmationMailOptions, function (error, response){
      if (error) {
        console.log("Confirmation message error: " + error);
      } else {
        console.log("Confirmation message sent: " + response.message);
      }
    });
    
  });

  return router;
};