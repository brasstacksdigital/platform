/**
 * routes/api/index.js
 */
 
module.exports = function(Client) {
  var router = require('express').Router();
  
  router.use('/emails', require('./emails')());
  
  router.use('/integrations', require('./integrations')(Client));
  
  router.use('/list_items', require('./listItems')());

  router.use('/sites', require('./sites')(Client));

  router.use('/uploads', require('./uploads')());

  return router;
};