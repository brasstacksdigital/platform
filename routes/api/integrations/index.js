/**
 * routes/api/integrations/index.js
 */
 
 module.exports = function(Client) {
  var router = require('express').Router();
  
  router.use('/instagram', require('./instagram')(Client));

  return router;
};