/**
 * routes/api/integrations/instagram/index.js
 */
var request = require('superagent'),
  instagramConfig = require('../../../../config/integrations.js').instagram;

module.exports = function (Client) {
  var router = require('express')
    .Router();

  router.get('/redirect', function (req, res) {
    if (req.query.error) {
      console.log("Error: " + req.query.error + "\nReason: " + req.query.error_reason +
        "\nDescription: " + req.query.error_description);
      return;
    }
    request
      .post('https://api.instagram.com/oauth/access_token')
      .send({
        client_id: instagramConfig.clientId,
        client_secret: instagramConfig.clientSecret,
        grant_type: "authorization_code",
        redirect_uri: instagramConfig.redirectUri + "?client_id=" + req
          .query.client_id,
        code: req.query.code
      })
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Accept', 'application/json')
      .end(function (err, res) {
        Client.findByIdAndUpdate(req.query.client_id, {
          $set: {
            'integrations.instagram.accessToken': res.body.access_token,
            'integrations.instagram.user': res.body.user
          }
        }, {
          upsert: true
        }, function (err, clientData) {
          if (err) {
            console.log("err", err);
            return;
          }
        })
      });
    res.redirect("/admin");
  });

  return router;
};
