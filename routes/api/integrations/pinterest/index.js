/**
 * routes/api/integrations/pinterest/index.js
 */
var request = require('superagent'),
  pinterestConfig = require('../../../../config/integrations.js').pinterest;

module.exports = function (Client) {
  var router = require('express')
    .Router();

  router.get('/redirect', function (req, res) {
    if (req.query.error) {
      console.log("Error: " + req.query.error + "\nReason: " + req.query.error_reason +
        "\nDescription: " + req.query.error_description);
      return;
    }
    request
      .post('https://api.pinterest.com/v1/oauth/token')
      .send({
        client_id: pinterestConfig.clientId,
        client_secret: pinterestConfig.clientSecret,
        grant_type: "authorization_code",
        code: req.query.code
      })
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Accept', 'application/json')
      .end(function (err, res) {
        console.log("res", res);
        /*Client.findByIdAndUpdate(req.query.client_id, {
          $set: {
            'integrations.pinterest.accessToken': res.body.access_token,
            'integrations.pinterest.user': res.body.user
          }
        }, {
          upsert: true
        }, function (err, clientData) {
          if (err) {
            console.log("err", err);
            return;
          }
        })*/
      });
    res.redirect("/admin");
  });

  return router;
};
