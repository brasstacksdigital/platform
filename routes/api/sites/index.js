/**
 * routes/api/sites/index.js
 */

module.exports = function (Client) {
  var router = require('express')
    .Router();

  router.post('/favicon', function (req, res) {
    console.log("post to favicon", req.body);
    Client.findByIdAndUpdate(req.body.clientId, {
      $set: {
        'site.favicon': req.body.faviconUrl
      }
    }, function (err, clientData) {
      if (err) {
        console.log("err", err);
        return;
      }
      console.log("clientData.site", clientData.site);
      res.status(200).json(clientData.site);
    });

  });

  return router;
};