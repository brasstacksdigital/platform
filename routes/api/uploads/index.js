/**
 * routes/api/uploads/index.js
 */

var aws = require('aws-sdk'),
 awsAccessKey = process.env.AWS_ACCESS_KEY,
 awsSecretKey = process.env.AWS_SECRET_KEY,
 s3Bucket= process.env.S3_BUCKET;

// Log aws responses
aws.config.logger = process.stdout;

module.exports = function() {
  var router = require('express').Router();
  
  router.get('/sign_s3', function(req, res){
    console.log("sign_s3");
    aws.config.update({accessKeyId: awsAccessKey, secretAccessKey: awsSecretKey});
    var s3 = new aws.S3();
    // Create unique file key based on timestamp and filename
    var fileKey = Date.now() + "-" + req.query.file_name;
    var s3Params = {
      Bucket: s3Bucket,
      Key: fileKey,
      Expires: 60,
      ContentType: req.query.file_type,
      ACL: "public-read"
    };
    s3.getSignedUrl("putObject", s3Params, function(err, data){
      if(err){
        console.log(err);
      }
      else{
        var returnData = {
          signedRequest: data,
          url: "https://" + s3Bucket + ".s3.amazonaws.com/" + fileKey
        };
        res.status(200).json(returnData);
      }
    });
  });

  return router;
};