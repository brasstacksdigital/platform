/**
 * routes/index.js
 */
var Client = require('../models/client'),
  Spreadsheet = require('edit-google-spreadsheet'),
  googleServiceAccount = require('../config/googleServiceAccount');

//Needed for onTouchTap
//Can go away when react 1.0 release
//Check this repo:
//https://github.com/zilverline/react-tap-event-plugin
var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

module.exports = function (passport) {
  var router = require('express')
    .Router();

  router.use('/admin', require('./admin')(Client));

  router.use('/api', require('./api')(Client));

  router.post('/login', function (req, res, next) {
    console.log("body", req.body);
    passport.authenticate('local-login', function (err, user, info) {
      console.log("user", user);
      console.log("info", info);
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.json({
          user: null,
          message: req.flash('loginMessage')
        });
      }
      req.logIn(user, function (err) {
        if (err) {
          return next(err);
        } else {
          res.redirect("admin");
          /*
          // Return specific user information
          return res.json({
            user: {
              id: user.id,
              email: user.local.email,
              isAdmin: user.local.isAdmin,
              token: user.local.token
            },
            message: "You were successfully logged in"
          });
          */
        }
      });
    })(req, res, next);
  });

  router.get('/logout', function (req, res) {
    req.logout();
    res.status(200)
      .send("You have been successfully logged out");
  });
  
  router.get('/shop', function (req, res) {
    Spreadsheet.load({
      debug: true,
      spreadsheetId: '1mTSHiSa3u1AG3FQK-YyRlw2FbluxKgPncVBJ1smCBPw',
      worksheetName: 'Inventory',
      oauth : {
          email: googleServiceAccount.clientEmail,
          keyFile: 'platform-store-key.pem'
      }
    }, function sheetReady(err, spreadsheet) {
      if (err) {
        console.log("err", err);
        res.status(500)
          .send("Unable to access spreadsheet db");
      }
      spreadsheet.receive(function(err, rows, info) {
        if (err) {
            throw err;
        }
        console.log("rows", rows);
        console.log("info", info);
      });
    });
  });

  router.use('/:path?', require('./site')(Client));

  return router;
};
