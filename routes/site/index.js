/**
 * routes/site/index.js
 */
var React = require('react'),
  App = React.createFactory(require('../../components/site/App')),
  JSON2 = require('JSON2'),
  utilities = require('../../utils/utilities');

module.exports = function (Client) {
  var router = require('express')
    .Router();

  router.get('/', function (req, res) {
    var host = req.get("X-Development") ? "development" : req.headers.host;
    utilities.fetchClientData(Client, host, function (clientData) {
      // Convert Mongoose object to regular JSON object before render
      clientData = clientData.toObject();
      var activeRoute = utilities.getActiveRoute(req.originalUrl, clientData.site.routes);
      console.log("activeRoute", activeRoute);
      // Add variables specific to app instance that are not saved in the db
      clientData.site.activeRoute = activeRoute
        // Add string id for use in client api requests
      clientData.id = clientData._id.toString();
      // If authenticated set user object
      if (req.isAuthenticated()) {
        clientData.site.activeUser = req.session.passport.user;
      } else {
        clientData.site.activeUser = null;
      }
      // Render React to a string
      var markup = React.renderToString(
        App(clientData)
      );
      // Set cache headers
      res.setHeader("Cache-Control", "public, max-age=" + (5 * 60));
      // Render our 'app' template
      res.render('app', {
        title: activeRoute.title,
        description: activeRoute.description,
        favicon: clientData.site.favicon,
        themeColor: clientData.site.colorPalette.backgroundColor,
        fonts: clientData.site.fonts,
        backgroundColor: clientData.site.colorPalette.backgroundColor || '#fff',
        linkColor: clientData.site.colorPalette.linkColor || '#808080',
        linkHoverColor: clientData.site.colorPalette.linkHoverColor || '#1a0dab',
        markup: markup, // Pass rendered react markup
        state: JSON2.stringify(JSON2.decycle(clientData)), // Use JSON2 library to handle circular references in nested components
        gaid: clientData.site.integrations.googleAnalytics.id
      });
    })
  });

  return router;
};
