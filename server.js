/**
 * Server side js
 */

// Dependencies
// Instantiate babel for es6 and jsx transforms on the server
require("babel/register");
var express = require('express'),
  mongoose = require('mongoose'),
  exphbs = require('express-handlebars'),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser'),
  passport = require('passport'),
  flash = require('connect-flash'),
  session = require('express-session'),
  MongoStore = require('connect-mongo')(session),
  secret = process.env.SECRET,
  configDB = require('./config/database');

var app = express();
app.engine('handlebars', exphbs({
  defaultLayout: __dirname + '/views/layouts/main',
  layoutsDir: __dirname + '/views/layouts'
}));
app.set('view engine', 'handlebars');
app.use(bodyParser()); // get information from html forms
app.use(cookieParser()); // read cookies (needed for auth)

// For MongoDB instance
mongoose.connect(configDB);

// Session config
app.use(session({
  store: new MongoStore({
    mongooseConnection: mongoose.connection
  }),
  cookie: {
    maxAge: 3600000 // expire the session(-cookie) after 1 hour
  },
  secret: secret
}));

// For Passport
require('./config/auth')(passport); // pass passport for authentication configuration
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// Log all requests before processing route
app.use(function (req, res, next) {
  console.log("req.path", req.path);
  console.log("req.session", req.session);
  next(); // Passing the request to the next handler in the stack.
});

// Base routes
app.use('/', express.static(__dirname + '/public'));
app.use('/', require('./routes')(passport));

app.listen(8000, '127.0.0.1');
