/**
 * AdminStore
 */
var AppDispatcher = require('../dispatcher/AppDispatcher'),
  EventEmitter = require('events')
  .EventEmitter,
  AdminConstants = require('../constants/AdminConstants'),
  assign = require('object-assign');

// Define initial admin data
var _adminConfig = {};

function loadAdminConfigData(data) {
  _adminConfig = data;
}

// Method to load initial admin data to populate store
function updateActiveView(view) {
  _adminConfig.activeView = view;
}

// Extend AdminStore with EventEmitter to add eventing capabilities
var AdminStore = assign({}, EventEmitter.prototype, {

  // Return admin data
  getAdminConfig: function () {
    return _adminConfig;
  },

  // Emit Change event
  emitChange: function () {
    this.emit('change');
  },

  // Add change listener
  addChangeListener: function (callback) {
    this.on('change', callback);
  },

  // Remove change listener
  removeChangeListener: function (callback) {
    this.removeListener('change', callback);
  }

});

// Register callback with AppDispatcher
AppDispatcher.register(function (payload) {
  var action = payload.action;

  switch (action.actionType) {

    // Respond to LOAD_ADMIN_CONFIG_DATA
    case AdminConstants.LOAD_ADMIN_CONFIG_DATA:
      loadAdminConfigData(action.data);
      // If action was responded to, emit change event
      AdminStore.emitChange();
      break;

      // Respond to UPDATE_ACTIVE_VIEW action
    case AdminConstants.UPDATE_ACTIVE_VIEW:
      updateActiveView(action.view);
      // If action was responded to, emit change event
      AdminStore.emitChange();
      break;

    default:
      return true;
  }

  return true;

});

module.exports = AdminStore;
