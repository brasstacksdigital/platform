/**
 * SiteStore
 */
var AppDispatcher = require('../dispatcher/AppDispatcher'),
  EventEmitter = require('events')
  .EventEmitter,
  SiteConstants = require('../constants/SiteConstants'),
  assign = require('object-assign'),
  utilities = require('../utils/utilities');

// Define initial site data
var _site = {};

// Method to load initial site data to populate store
function loadSiteData(site) {
  _site = site;
}

// Method to update active route in site
function updateActiveRoute(route) {
  _site.activeRoute = utilities.getActiveRoute(route, _site.routes);
  // Update url path
  window.history.pushState({
    route: route
  }, "", route);
  // Set onpopstate function for use in browser navigation
  window.onpopstate = function (e) {
    _site.activeRoute = e.state.route;
    SiteStore.emitChange();
  };
  window.scrollTo(0, 0);
  // Update title and description of page
  document.getElementsByTagName("title")[0].innerHTML = _site.activeRoute.title;
  document.getElementsByName("description")[0].innerHTML = _site.activeRoute.description;
}

function setActiveUser(user) {
  _site.activeUser = user;
}

// Extend SiteStore with EventEmitter to add eventing capabilities
var SiteStore = assign({}, EventEmitter.prototype, {

  // Return site data
  getSite: function () {
    return _site;
  },

  // Return integration data
  getIntegrations: function () {
    return _site.integrations;
  },

  // Emit Change event
  emitChange: function () {
    this.emit('change');
  },

  // Add change listener
  addChangeListener: function (callback) {
    this.on('change', callback);
  },

  // Remove change listener
  removeChangeListener: function (callback) {
    this.removeListener('change', callback);
  }

});

// Register callback with AppDispatcher
AppDispatcher.register(function (payload) {
  var action = payload.action;

  switch (action.actionType) {

    // Respond to LOAD_SITE_DATA action
    case SiteConstants.LOAD_SITE_DATA:
      loadSiteData(action.site);
      // If action was responded to, emit change event
      SiteStore.emitChange();
      break;

      // Respond to UPDATE_ACTIVE_ROUTE action
    case SiteConstants.UPDATE_ACTIVE_ROUTE:
      updateActiveRoute(action.route);
      // If action was responded to, emit change event
      SiteStore.emitChange();
      break;

      // Respond to SET_ACTIVE_USER action
    case SiteConstants.SET_ACTIVE_USER:
      setActiveUser(action.user);
      // If action was responded to, emit change event
      SiteStore.emitChange();
      break;

    default:
      return true;
  }

  return true;

});

module.exports = SiteStore;
