/**
 * utils/utilities.js
 */

module.exports = {
  /**
   * Function to match route to path in routes and set active route object
   * @param {string} path
   * @param {array} routes
   * @return {object}
   */
  getActiveRoute: function (path, routes) {
    // Loop through routes to find active route
    var routeFound = false,
      activeRoute = {};
    // First check for special hardcoded routes
    if (path === "/admin") {
      return {
        path: "/admin",
        title: "Admin",
        description: "Admin panel for BrassTacksDigital.com site"
      }
    }
    if (path === "/login") {
      return {
        path: "/login",
        title: "Login",
        description: "Login page for BrassTacksDigital.com site"
      }
    }
    for (var i = 0; i < routes.length; i++) {
      if (path === routes[i].path) {
        routeFound = true;
        activeRoute = routes[i];
        break;
      }
    }
    if (!routeFound) {
      activeRoute = {
        path: "/error",
        title: "Error",
        description: "Error page for BrassTacksDigital.com site",
        error: {
          status: 404,
          message: "Oops looks like we couldn't find that route"
        }
      }
    }
    return activeRoute;
  },

  /**
   * Function to fetch client data based on host
   * Pass Client model to avoid error when requiring model more than once
   * @param {object} Client
   */
  fetchClientData: function (Client, host, callack) {
    Client.findOne({
        "site.hostname": host
      })
      .populate({
        path: "site.routes",
      })
      .exec(function (err, clientData) {
        if (err) {
          console.log("err", err);
          return;
        }
        callack(clientData);
      });
  }
}
