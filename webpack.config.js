module.exports = {
  entry: './client.jsx',
  output: {
    filename: "./public/js/bundle.js"
  },
  module: {
    loaders: [
	    {
	      test: /\.js?$/,
	      loader: 'babel-loader',
	      exclude: /node_modules/
	    },
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }
	  ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  }
};